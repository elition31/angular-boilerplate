import { Component } from '@angular/core';
import { Loader, LoaderService } from 'src/app/core/services/loader.service';

@Component({
  selector: 'app-loaders',
  templateUrl: './loaders.component.html',
  styleUrls: ['./loaders.component.scss'],
})
export class LoadersComponent {
  public loaders: Loader[] = [];

  constructor(private loaderService: LoaderService) {
    this.loaderService.getLoaders().subscribe((loaders) => {
      this.loaders = loaders;
    });
  }
}
