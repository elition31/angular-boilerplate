import { Component, Input, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { INavigationValue } from "src/app/core/interfaces/navigation.interface";
import { Navigation } from "src/app/core/models/navigation/navigation";

@Component({
  selector: "app-h-navigation-items",
  templateUrl: "./h-navigation-items.component.html",
  styleUrls: ["./h-navigation-items.component.scss"],
})
export class HNavigationItemsComponent implements OnInit {
  @Input() public navigation!: Navigation;
  @Input() public sectionsNumber!: number;
  @Input() public position!: "start" | "end" | "center";

  constructor(private router: Router) {}

  ngOnInit(): void {}

  public navigateTo(destination: string): void {
    // this.toggleMenu();
    this.router.navigateByUrl(destination);
  }

  public getontent(): INavigationValue[] {
    if (this.sectionsNumber === 3) {
      return this.navigation.getContentForNavigationTemplateThreeSections(this.position);
    }

    if (this.sectionsNumber === 2) {
      this.position = this.position === "center" ? "start" : this.position;
      return this.navigation.getContentForNavigationTemplateTwoSections(this.position);
    }

    if (this.sectionsNumber === 1) {
      return this.navigation.getContentForNavigationTemplateOneSection();
    }

    return [];
  }
}
