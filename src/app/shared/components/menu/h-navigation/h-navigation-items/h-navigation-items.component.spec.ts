import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HNavigationItemsComponent } from './h-navigation-items.component';

describe('HNavigationItemsComponent', () => {
  let component: HNavigationItemsComponent;
  let fixture: ComponentFixture<HNavigationItemsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HNavigationItemsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HNavigationItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
