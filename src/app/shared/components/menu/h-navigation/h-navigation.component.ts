import { Component, Input, OnInit } from "@angular/core";
import { Navigation } from "src/app/core/models/navigation/navigation";
import { INavigationTemplateContent, INavigationTemplateOneSection, INavigationTemplateThreeSections, INavigationTemplateTwoSection } from "src/app/core/interfaces/navigation.interface";
import { Router } from "@angular/router";

@Component({
  selector: "app-h-navigation",
  templateUrl: "./h-navigation.component.html",
  styleUrls: ["./h-navigation.component.scss"],
})
export class HNavigationComponent implements OnInit {
  @Input() public navigation!: Navigation;

  constructor(private router: Router) { }

  ngOnInit(): void { }

  public navigateTo(destination: string): void {
    // this.toggleMenu();
    this.router.navigateByUrl(destination);
  }
}
