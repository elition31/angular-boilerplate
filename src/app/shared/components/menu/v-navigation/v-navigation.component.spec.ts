import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VNavigationComponent } from './v-navigation.component';

describe('VNavigationComponent', () => {
  let component: VNavigationComponent;
  let fixture: ComponentFixture<VNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
