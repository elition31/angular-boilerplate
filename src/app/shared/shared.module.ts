// Angular
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// Components
import { ToastsComponent } from './components/toasts/toasts.component';
import { ToastComponent } from './components/toasts/toast/toast.component';
import { LoadersComponent } from './components/loaders/loaders.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { HNavigationComponent } from './components/menu/h-navigation/h-navigation.component';
import { VNavigationComponent } from './components/menu/v-navigation/v-navigation.component';
import { LogoComponent } from './components/logo/logo.component';

// Material Modules
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatInputModule } from "@angular/material/input";
import { MatProgressSpinnerModule } from "@angular/material/progress-spinner";
import { MatSelectModule } from "@angular/material/select";
import { MatProgressBarModule } from "@angular/material/progress-bar";

// Directives
import { DropZoneDirective } from './directives/dropzone.directive';
import { HNavigationItemsComponent } from './components/menu/h-navigation/h-navigation-items/h-navigation-items.component';

const MATERIAL_MODULE = [MatFormFieldModule, MatInputModule, MatProgressSpinnerModule, MatSelectModule, MatProgressBarModule];

const MODULES = [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, ...MATERIAL_MODULE];

const COMPONENTS = [
  ToastsComponent,
  ToastComponent,
  LoadersComponent,
  SpinnerComponent,
  HNavigationComponent,
  VNavigationComponent,
  LogoComponent,
];

const DIRECTIVES = [DropZoneDirective];

const MODALS: any = [];

@NgModule({
  declarations: [...COMPONENTS, ...DIRECTIVES, ...MODALS, HNavigationItemsComponent],
  imports: [...MODULES],
  exports: [...MODULES, ...COMPONENTS, ...DIRECTIVES, ...MODALS],
  providers: [],
})
export class SharedModule {}
