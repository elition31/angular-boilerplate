import { NgModule } from '@angular/core';
import { GuestLayoutComponent } from './guest-layout.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [GuestLayoutComponent],
  imports: [SharedModule],
  exports: [SharedModule, GuestLayoutComponent],
})
export class GuestLayoutModule {}
