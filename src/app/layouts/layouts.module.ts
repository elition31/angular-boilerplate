import { NgModule } from '@angular/core';
import { GuestLayoutModule } from './guest-layout/guest-layout.module';

@NgModule({
  declarations: [],
  imports: [GuestLayoutModule],
  exports: [GuestLayoutModule],
})
export class LayoutsModule {}
