import { Component } from "@angular/core";
import { INavigationTemplateThreeSections } from "./core/interfaces/navigation.interface";
import { Navigation } from "./core/models/navigation/navigation";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  private content: INavigationTemplateThreeSections = {
    start: {
      position: "start",
      values: [
        { value: "Accueil", link: "/", isLogo: true },
        { value: "Page 0", link: "/titi", isLogo: false, icon: { isEnabled: true, value: "fas fa-home"} },
      ],
    },
    center: {
      position: "center",
      values: [
        { value: "Page 1", link: "/toto", isLogo: false },
        { value: "Page 2", link: "/tata", isLogo: false },
      ],
    },
    end: {
      position: "end",
      values: [
        { value: "Page 3", link: "/toto", isLogo: false, icon: { isEnabled: true, value: "fas fa-user-circle"} },
        { value: "Page 4", link: "/tata", isLogo: false, icon: { isEnabled: true, value: "fas fa-cogs"} },
      ],
    },
  };

  public nav: Navigation = new Navigation("1-1-1", "start", true, this.content);
}
