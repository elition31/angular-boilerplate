// Angular
import { Injectable } from "@angular/core";

// Libraries
import { BehaviorSubject } from "rxjs";

export interface Loader {
  id?: string;
  state?: boolean;
  message?: string;
  displayMessage?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private loaders$: BehaviorSubject<Loader[]> = new BehaviorSubject<Loader[]>([]);

  constructor() { }

  /**
   * Method called by the layout to fetch the current loaders
   */
  public getLoaders(): BehaviorSubject<Loader[]> {
    return this.loaders$;
  }

  /**
   * Method called to create a new loader
   */
  public addLoader(loader: Loader): string {
    loader.id = this.generateLoaderId();
    this.loaders$.next([...this.loaders$.value, loader]);
    return loader.id;
  }

  /**
   * Generate a unique ID
   */
  private generateLoaderId(): string {
    return `${Date.now() + Math.random()}`;
  }

  /**
   * Delete manually the toast from the queue
   */
  public deleteLoaderById(id: string): void {
    const values: Loader[] = this.loaders$.value;
    values.splice(values.findIndex(v => v.id === id), 1);
    this.loaders$.next(values);
  }
}
