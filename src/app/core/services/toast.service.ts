// Angular
import { Injectable } from "@angular/core";

// Libraries
import { BehaviorSubject } from "rxjs";

// Models
import { Toast } from "../interfaces/toast.interface";

@Injectable({
  providedIn: "root",
})
export class ToastService {
  private toasts: BehaviorSubject<Toast[]> = new BehaviorSubject<Toast[]>([]);
  public position: "left" | "right" = "left";

  constructor() { }

  /**
   * Method called by the layout to fetch the current toasts
   */
  public getToasts(): BehaviorSubject<Toast[]> {
    return this.toasts;
  }

  /**
   * Method called to create a new toast
   */
  public addToast(toast: Toast): void {
    toast.id = this.generateToastId();
    this.toasts.next([...this.toasts.value, toast]);
    this.removeToastWhenTimeIsOver(toast);
  }

  /**
   * Generate a unique ID
   */
  private generateToastId(): string {
    return `${Date.now() + Math.random()}`;
  }

  /**
   * Delete manually the toast from the queue
   */
  public deleteToastById(id: string): void {
    const values: Toast[] = this.toasts.value;
    values.splice(values.findIndex(v => v.id === id), 1);
    this.toasts.next(values);
  }

  /**
   * Remove the toast from the queue when the timer is done
   */
  private removeToastWhenTimeIsOver(toast: Toast): void {
    setTimeout(() => {
      if (this.toasts.value.find(v => v.id === toast.id) !== undefined) {
        const values: Toast[] = this.toasts.value;
        values.splice(values.findIndex(v => v.id === toast.id), 1);
        this.toasts.next(values);
      }
    }, toast.duration + 600);
  }
}
