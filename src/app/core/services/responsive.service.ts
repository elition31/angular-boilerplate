import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { BreakpointObserver } from "@angular/cdk/layout";

@Injectable({
  providedIn: "root",
})
export class ResponsiveService {
  private isMobile: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private breakpointObserver: BreakpointObserver) {
    this.breakpointObserver.observe("(max-width: 900px)").subscribe((state) => {
      this.isMobile.next(state.matches);
    });
  }

  public getState(): BehaviorSubject<boolean> {
    return this.isMobile;
  }
}
