export const distinct = (array: any[]): any[] => {
  return array.filter((value, index, self) => { return self.indexOf(value) === index; });
}