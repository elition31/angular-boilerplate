import {
  INavigation,
  INavigationTemplateOneSection,
  INavigationTemplateThreeSections,
  INavigationTemplateTwoSection,
  INavigationValue,
} from "../../interfaces/navigation.interface";

export class Navigation implements INavigation {
  template: "1-1-1" | "2-1" | "1-2" | "1";
  burgerMenuIconPosition: "start" | "end";
  isLogoDisplayedOnMobile: boolean;
  content: INavigationTemplateThreeSections | INavigationTemplateTwoSection | INavigationTemplateOneSection;
  isMenuDisplayed: boolean = false;

  constructor(
    template: "1-1-1" | "2-1" | "1-2" | "1",
    burgerMenuIconPosition: "start" | "end",
    isLogoDisplayedOnMobile: boolean,
    content: INavigationTemplateThreeSections | INavigationTemplateTwoSection | INavigationTemplateOneSection
  ) {
    this.template = template;
    this.burgerMenuIconPosition = burgerMenuIconPosition;
    this.isLogoDisplayedOnMobile = isLogoDisplayedOnMobile;
    this.content = content;
  }

  /**
   * This method returns the grid template
   */
  public getGridTemplate(): string {
    let gridTemplate: string = "";

    switch (this.template) {
      case "1":
        gridTemplate = "1fr";
        break;

      case "1-2":
        gridTemplate = "1fr 2fr";
        break;

      case "2-1":
        gridTemplate = "2fr 1fr";
        break;

      case "1-1-1":
        gridTemplate = "1fr 1fr 1fr";
        break;
    }

    return gridTemplate;
  }

  public getContentPosition(sections: 1 | 2 | 3, key?: "start" | "center" | "end"): string {
    if (sections === 1) {
      if (this.isNavigationTemplateOneSection(this.content)) {
        return this.content.center.position;
      }
    }

    if (sections === 2) {
      key = key === "center" ? "start" : key;
      if (this.isNavigationTemplateTwoSections(this.content)) {
        return this.content[key!].position;
      }
    }

    if (sections === 3) {
      if (this.isNavigationTemplateThreeSections(this.content)) {
        return this.content[key!].position;
      }
    }

    return "center";
  }

  public getContentForNavigationTemplateThreeSections(key: "start" | "center" | "end"): INavigationValue[] {
    if (this.isNavigationTemplateThreeSections(this.content)) {
      return this.content[key].values;
    } else {
      return [];
    }
  }

  public getContentForNavigationTemplateTwoSections(key: "start" | "end"): INavigationValue[] {
    if (this.isNavigationTemplateTwoSections(this.content)) {
      return this.content[key].values;
    } else {
      return [];
    }
  }

  public getContentForNavigationTemplateOneSection(): INavigationValue[] {
    if (this.isNavigationTemplateOneSection(this.content)) {
      return this.content.center.values;
    } else {
      return [];
    }
  }

  public isNavigationTemplateThreeSections(value: INavigationTemplateThreeSections | INavigationTemplateTwoSection | INavigationTemplateOneSection): value is INavigationTemplateThreeSections {
    return (<INavigationTemplateThreeSections>value).start !== undefined;
  }

  public isNavigationTemplateTwoSections(value: INavigationTemplateThreeSections | INavigationTemplateTwoSection | INavigationTemplateOneSection): value is INavigationTemplateTwoSection {
    return (<INavigationTemplateTwoSection>value).start !== undefined;
  }

  public isNavigationTemplateOneSection(value: INavigationTemplateThreeSections | INavigationTemplateTwoSection | INavigationTemplateOneSection): value is INavigationTemplateOneSection {
    return (<INavigationTemplateOneSection>value).center !== undefined;
  }
}
