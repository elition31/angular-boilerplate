export interface INavigation {
  template: "1-1-1" | "2-1" | "1-2" | "1";
  burgerMenuIconPosition: "start" | "end";
  isLogoDisplayedOnMobile: boolean;
  content: INavigationTemplateThreeSections | INavigationTemplateTwoSection | INavigationTemplateOneSection;
  isMenuDisplayed: boolean;
}

export interface INavigationTemplateThreeSections {
  start: INavigationTemplateContent;
  center: INavigationTemplateContent;
  end: INavigationTemplateContent;
}

export interface INavigationTemplateTwoSection {
  start: INavigationTemplateContent;
  end: INavigationTemplateContent;
}

export interface INavigationTemplateOneSection {
  center: INavigationTemplateContent;
}

export interface INavigationTemplateContent {
  position: "start" | "end" | "center";
  values: INavigationValue[];
}

export interface INavigationValue {
  value: string;
  link: string;
  isLogo: boolean;
  icon?: {
    isEnabled: boolean;
    value: string;
  };
}
